#version 300 es

precision highp float;

uniform sampler2D u_texture;

in vec4 v_color;
in vec2 v_texCoord;

uniform vec2 u_resolution;
uniform float u_smoothing;

out vec4 out_color;

void main() {
	vec2 resolution = u_resolution;
	vec4 center = texture(u_texture, v_texCoord);
	vec4 color = vec4(0.0);
	float total = 0.0;

	float samples = u_smoothing;

	for (float x = -samples; x <= samples; x += 1.0) {
		for (float y = -samples; y <= samples; y += 1.0) {
			vec4 pixel = texture(u_texture, v_texCoord + vec2(x, y) / vec2(resolution));
			float weight = 1.0 - abs(dot(pixel.rgb - center.rgb, vec3(0.25)));
			weight = pow(weight, 5.0);
			color += pixel * weight;
			total += weight;
		}
	}

	out_color = color / total;
}
