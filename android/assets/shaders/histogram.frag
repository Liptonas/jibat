#version 300 es

precision highp float;

uniform sampler2D u_texture;

in vec4 v_color;
in vec2 v_texCoord;

out vec4 out_color;

void main() {
    ivec2 size = textureSize(u_texture, 0);
    vec2 texel = vec2(1.0 / float(size.x), 1.0 / float(size.y));

    float sum = 0.0;
    float count;
    for(float i = 0.0; i < float(size.x); i += 1.0) {
        vec2 uvs = vec2(mix(-1.0, 1.0, (texel.x * i)), 1.0 - v_texCoord.y);

        sum += texture(u_texture, uvs).r;
    }

	out_color = vec4( vec3(sum / float(size.x)), 1.0 );
}
