#version 300 es

precision highp float;

uniform sampler2D u_texture;

in vec4 v_color;
in vec2 v_texCoord;

out vec4 out_color;

float contour(in float d, in float w) {
    // smoothstep(lower edge0, upper edge1, x)
    return smoothstep(0.5 - w, 0.5 + w, d);
}

float samp(in vec2 uv, float w) {
    return contour(texture(u_texture, uv).a, w);
}

void main() {
    float dist = texture(u_texture, v_texCoord).a;
	float width = fwidth(dist);

	float alpha = contour(dist, width);

	float dscale = 0.354;
	vec2 duv = dscale * (dFdx(v_texCoord) + dFdy(v_texCoord));
	vec4 box = vec4(v_texCoord - duv, v_texCoord + duv);

	float sum = samp(box.xy, width) + samp(box.zw, width) + samp(box.xw, width) + samp(box.zy, width);

	alpha = (alpha + 0.5 * sum) / 3.0;

	out_color = vec4(v_color.rgb, alpha);
}
