#version 300 es

precision highp float;

uniform sampler2D u_texture;

in vec4 v_color;
in vec2 v_texCoord;

out vec4 out_color;

void make_kernel(inout vec4 n[9], sampler2D tex, vec2 coord)
{
    ivec2 size = textureSize(tex, 0);

	float w = 1.0 / float(size.x);
	float h = 1.0 / float(size.y);

	n[0] = texture(tex, coord + vec2( -w, -h));
	n[1] = texture(tex, coord + vec2(0.0, -h));
	n[2] = texture(tex, coord + vec2(  w, -h));
	n[3] = texture(tex, coord + vec2( -w, 0.0));
	n[4] = texture(tex, coord);
	n[5] = texture(tex, coord + vec2(  w, 0.0));
	n[6] = texture(tex, coord + vec2( -w, h));
	n[7] = texture(tex, coord + vec2(0.0, h));
	n[8] = texture(tex, coord + vec2(  w, h));
}

void main() {
    vec4 n[9];
	make_kernel( n, u_texture, v_texCoord );

	vec4 sobel_edge_h = n[2] + (2.0*n[5]) + n[8] - (n[0] + (2.0*n[3]) + n[6]);
  	vec4 sobel_edge_v = n[0] + (2.0*n[1]) + n[2] - (n[6] + (2.0*n[7]) + n[8]);
	vec4 sobel = sqrt((sobel_edge_h * sobel_edge_h) + (sobel_edge_v * sobel_edge_v));

	vec3 clamped =  clamp(pow(sobel.rgb, vec3(1.6)), 0.0, 1.0);
	//clamped = pow(clamped, vec3(0.5));

	float leeway = 1.2;

	float grayscale = dot(clamped.rgb, vec3(0.299, 0.587, 0.114));

	grayscale = pow(grayscale, 1.0);

	if(grayscale > 0.08) {
		//grayscale = 1.0;
	}

	if(clamped.r * leeway < 1.0 && clamped.g * leeway < 1.0 && clamped.b * leeway < 1.0) {
		//clamped.rgb = 1.0 - vec3(1.0, 1.0, 1.0);
	}

	out_color = vec4( vec3(grayscale), 1.0 );
}
