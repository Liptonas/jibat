package com.skrebulai.obunga;

public class Tuple<T> {
	public final T o1;
	public final T o2;
	
	public Tuple(T o1, T o2) {
		this.o1 = o1;
		this.o2 = o2;
	}
	
	public boolean isComplete() {
		return (o1 != null & o2 != null) && (o1 != o2);
	}
}
