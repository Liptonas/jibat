package com.skrebulai.obunga;

public class Mark {
	private float fraction;
	private float value;
	
	public Mark(float fraction, float value) {
		this.fraction = fraction;
		this.value = value;
	}
	
	public Mark() {
		this(0.0f, 0.0f);
	}
	
	public void setFraction(float fraction) {
		this.fraction = fraction;
	}
	
	public void setValue(float value) {
		this.value = value;
	}
	
	public float getFraction() {
		return this.fraction;
	}
	
	public float getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		return "[fraction => " + this.fraction + " - value => " + this.value + "]";
	}
}
