package com.skrebulai.obunga;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.math.MathUtils;

public class Ladder {
	private Texture texture;
	private Pixmap pixmap;
	private ArrayList<Mark> marks;
	
	public Ladder() {
		marks = new ArrayList<Mark>();
	}
	
	public void loadFromDescriptorFile(FileHandle file) {
		marks.clear();
		
		String config = file.readString();
		String contents[] = config.split("\n");
		
		updateTexture(new Texture(file.path().substring(0, file.path().lastIndexOf("/") + 1) + contents[0].trim()));
		
		for (int i = 1; i < contents.length; i++) {
			String descriptor = contents[i].trim();
			String[] args = descriptor.split("=");

			float fraction = (1.0f - Float.parseFloat(args[0].trim()));
			float value = Float.parseFloat(args[1].trim());

			Mark mark = new Mark(fraction, value);
			marks.add(mark);
		}
		
		sortMarks();
	}
	
	public void loadTexture(FileHandle handle) {
		updateTexture(new Texture(handle));
	}
	
	public void sortMarks() {
		Collections.sort(marks, new Comparator<Mark>() {

			@Override
			public int compare(Mark arg0, Mark arg1) {
				return Float.compare(arg1.getFraction(), arg0.getFraction());
			}
		});
	}
	
	public Tuple<Mark> getBounds(float fraction) {
		Mark min = null;
		Mark max = null;
		
		int index = 0;
		for(Mark mark : marks) {
			if(fraction >= mark.getFraction() && max == null) {
				min = marks.get(MathUtils.clamp(index -= 1, 0, marks.size()));
				max = mark;
			}
			
			index++;
		}		
		
		return new Tuple<Mark>(min, max);
	}
	
	public Texture getTexture() {
		return texture;
	}
	
	public ArrayList<Mark> getMarks() {
		return marks;
	}
	
	public Pixmap getPixmap() {
		return pixmap;
	}
	
	private void updateTexture(Texture texture) {
		if(this.texture != null)
			this.texture.dispose();
		
		this.texture = texture;
		this.texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		this.texture.getTextureData().prepare();
		
		if(this.pixmap != null)
			this.pixmap.dispose();
		
		this.pixmap = this.texture.getTextureData().consumePixmap();
	}
	
	/*@Override
	public String toString() {
		return "";
	}*/
}
