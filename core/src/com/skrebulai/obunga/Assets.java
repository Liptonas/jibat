package com.skrebulai.obunga;

import java.awt.peer.LightweightPeer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public class Assets {
	public static class Shaders {
		public static ShaderProgram textShader;
		public static ShaderProgram sobelShader;
		public static ShaderProgram blurShader;
		public static ShaderProgram histogramShader;
		
		private static void Load() {
			ShaderProgram.pedantic = false;
			
			textShader = LoadShader("Distance Field", "distanceField");
			sobelShader = LoadShader("Sobel", "batch", "sobel");
			blurShader = LoadShader("Sobel", "batch", "expBlur");
			histogramShader = LoadShader("Histogram", "batch", "histogram");			
		}
		
		private static ShaderProgram LoadShader(String name, String vert, String frag) {
			ShaderProgram shader = new ShaderProgram(Gdx.files.internal("shaders/" + vert + ".vert"), Gdx.files.internal("shaders/" + frag + ".frag"));
			if(!shader.isCompiled()) {
				System.out.println("Shader [" + name + "] compilation error:");
				System.out.println(shader.getLog());
			}
			
			return shader;
		}
		
		private static ShaderProgram LoadShader(String name, String names) {
			ShaderProgram shader = new ShaderProgram(Gdx.files.internal("shaders/" + names + ".vert"), Gdx.files.internal("shaders/" + names + ".frag"));
			if(!shader.isCompiled()) {
				System.out.println("Shader [" + name + "] compilation error:");
				System.out.println(shader.getLog());
			}
			
			return shader;
		}
	}
	
	public static class Fonts {
		public static BitmapFont lightFont;
		
		private static void Load() {
			{
				Texture texture = new Texture(Gdx.files.internal("fonts/light.png"), false);
				texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
				
				lightFont = new BitmapFont(Gdx.files.internal("fonts/light.fnt"), new TextureRegion(texture), false);
				lightFont.getData().setScale(0.60f);
			}
		}
	}
	
	public static void Load() {
		Shaders.Load();
		Fonts.Load();
	}
}
