package com.skrebulai.obunga;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class LadderRenderer {
	private Vector2 position;
	private Vector2 size;
	private Vector2 ratio;
	
	private Ladder ladder;
	
	private Rectangle bounds;
	
	public FrameBuffer processed;
	
	public LadderRenderer(Ladder ladder) {
		this.ladder = ladder;
		
		this.position = new Vector2(1, 1);
		this.size = new Vector2(1, 1);
		this.ratio = new Vector2(1, 1);
		
		this.bounds = new Rectangle(1, 1, 1, 1);
	}
	
	public void render(SpriteBatch batch) {
		batch.draw(ladder.getTexture(), position.x, position.y, size.x, size.y);
	}
	
	public Vector2 getScaledCoords(Vector2 fractions) {
		return new Vector2(this.size.y * fractions.x, this.size.y * fractions.y);
	}
	
	private void update() {
		this.ratio.y = this.size.x / this.ladder.getTexture().getWidth();
		this.ratio.y = this.size.y / this.ladder.getTexture().getHeight();
		
		this.bounds.set(position.x, position.y, size.x, size.y);
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
		update();
	}

	public Vector2 getSize() {
		return size;
	}

	public void setSize(Vector2 size) {
		this.size = size;
		update();
	}

	public Vector2 getRatio() {
		return ratio;
	}

	public Ladder getLadder() {
		return ladder;
	}

	public Rectangle getBounds() {
		return bounds;
	}
}
