package com.skrebulai.obunga;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import javax.swing.plaf.FileChooserUI;

import org.omg.CORBA.Bounds;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.VisUI.SkinScale;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSlider;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisWindow;
import com.kotcrab.vis.ui.widget.file.FileChooser;
import com.kotcrab.vis.ui.widget.file.FileChooser.Mode;
import com.kotcrab.vis.ui.widget.file.FileChooser.SelectionMode;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;
import com.kotcrab.vis.ui.widget.file.FileTypeFilter;
import com.skrebulai.obunga.Assets.Fonts;
import com.skrebulai.obunga.Assets.Shaders;

import net.spookygames.gdx.nativefilechooser.NativeFileChooser;
import net.spookygames.gdx.nativefilechooser.NativeFileChooserCallback;
import net.spookygames.gdx.nativefilechooser.NativeFileChooserConfiguration;

public class Main extends ApplicationAdapter {
	public static final int VIRTUAL_WIDTH = 1280;
	public static final int VIRTUAL_HEIGHT = 900;
	
	String refName;
	String urefName;
	
	boolean setupComparison = false;
	boolean setupLinks = false;
	
	Vector2 dragStart = new Vector2();
	Vector2 zoneDragStart = new Vector2();
	Vector2 originalZoneOffset = new Vector2();
	Rectangle newZoneBounds = new Rectangle();
	Rectangle currentDrag = null;
	
	boolean dragging = false;
	boolean selectingArea = false;
	boolean cancelFirst = false;
	boolean choosingRegions = false;
	
	Vector2 mousePos = new Vector2();

	SpriteBatch batch;
	ShapeRenderer sr;
	OrthographicCamera camera;
	
	LadderRenderer referenceLadder;
	Ladder userReferenceLadder = new Ladder();
	Texture userLadder;
	TextureRegion cutRegion;
	ArrayList<Rectangle> chosenRegions;
	ArrayList<TextureRegion> ladderRegions;
	ArrayList<Rectangle> computedRegions;
	
	ArrayList<Mark> bookmarks;
	
	ArrayList<Vector3> regionsOfInterest;
	ArrayList<FrameBuffer> processedLadders;
	ArrayList<FrameBuffer> ladderHistograms;
	
	BitmapFont lightFont;
	
	ArrayList<LadderRenderer> ladders;
	ArrayList<MarkLink> links;
	
	int currentMarkIndex = 0;
	
	Stage stage;
	Skin skin;
	
	VisWindow window;
	
	float smoothingValue = 1.0f;
	
	boolean takeScreenshot = false;
		
	class MarkLink {
		float value;
		
		float x;
		float x2;
		float x3;
		float x4;
		float y;
		float y2;
		
		public void renderLink(ShapeRenderer sr) {
			sr.line(x, y, x2, y);
			sr.line(x2, y, x3, y2);
			sr.line(x3, y2, x4 + 80, y2);
		}
		
		public void renderText(SpriteBatch batch) {
			Fonts.lightFont.draw(batch, String.valueOf(value), x4 + 10, y2 + 25);
		}
	}

	public Main(NativeFileChooser fileChooser) {
		//this.fileChooser = fileChooser;
	}

	@Override
	public void create() {		
		Assets.Load();
		
		batch = new SpriteBatch();
		sr = new ShapeRenderer();
		sr.setAutoShapeType(true);

		camera = new OrthographicCamera(VIRTUAL_WIDTH, VIRTUAL_HEIGHT);
		camera.setToOrtho(false, VIRTUAL_WIDTH, VIRTUAL_HEIGHT);	
		
		ladders = new ArrayList<LadderRenderer>();
		links = new ArrayList<Main.MarkLink>();
		ladderRegions = new ArrayList<TextureRegion>();
		chosenRegions = new ArrayList<Rectangle>();
		processedLadders = new ArrayList<FrameBuffer>();
		ladderHistograms = new ArrayList<FrameBuffer>();
		regionsOfInterest = new ArrayList<Vector3>();
		computedRegions = new ArrayList<Rectangle>();
		bookmarks = new ArrayList<Mark>();

		lightFont = Fonts.lightFont;
		
		VisUI.load(SkinScale.X1);
		
		stage = new Stage(new ScreenViewport());
		window = new VisWindow("Menu");
		
		ladders.add(null);
		
		final VisLabel refLadderLabel = new VisLabel("Current: ");	
		VisTextButton refLadderButton = new VisTextButton("Choose Reference Ladder");
		refLadderButton.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				FileChooser fileChooser = new FileChooser(Mode.OPEN);
				fileChooser.setSelectionMode(SelectionMode.FILES);
				fileChooser.setDirectory(Gdx.files.internal("refs"));
				
				FileTypeFilter filter = new FileTypeFilter(false);
				filter.addRule("Ladder Definition File (*.ldf)", ".ldf");
				
				fileChooser.setFileTypeFilter(filter);
				fileChooser.setListener(new FileChooserAdapter() {
					@Override
					public void selected (Array<FileHandle> file) {
						currentMarkIndex = 0;
						links.clear();
						
						Ladder l = new Ladder();						
						ladders.set(0, referenceLadder = new LadderRenderer(l));
						l.loadFromDescriptorFile(file.first());
						
						referenceLadder.setPosition(new Vector2(0, 0));
						referenceLadder.setSize(new Vector2(VIRTUAL_WIDTH * 0.12f, VIRTUAL_HEIGHT));
						
						refLadderLabel.setText("Current: " + file.get(0).name());
						refName = file.get(0).name();
						window.pack();
					}
				});
				fileChooser.fadeIn();
				stage.addActor(fileChooser);
			}
		});
		
		final VisLabel refActLadderLabel = new VisLabel("Current: ");	
		VisTextButton refActLadderButton = new VisTextButton("Choose User Comparison Picture");
		refActLadderButton.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				FileChooser fileChooser = new FileChooser(Mode.OPEN);
				fileChooser.setSelectionMode(SelectionMode.FILES);
				fileChooser.setDirectory(Gdx.files.internal("refs"));
				
				FileTypeFilter filter = new FileTypeFilter(false);
				filter.addRule("Image File (*.png, *.jpg, *.jpeg, *tif)", "png", "jpg", "jpeg", "tif");
				
				fileChooser.setFileTypeFilter(filter);
				fileChooser.setListener(new FileChooserAdapter() {
					@Override
					public void selected (Array<FileHandle> file) {
						currentMarkIndex = 0;
						links.clear();
						
						if(userLadder != null) {
							userLadder.dispose();
						}
						
						if(cutRegion != null) {
							cutRegion = null;
						}
						
						for(FrameBuffer fb : processedLadders) {
							fb.dispose();
						}
						
						userLadder = new Texture(file.first());
						
						refActLadderLabel.setText("Current: " + file.get(0).name());
						urefName = file.get(0).name();
						window.pack();
						
						setupComparison = true;
						selectingArea = true;
						cancelFirst = true;
						choosingRegions = false;
						
						ladderRegions.clear();
						chosenRegions.clear();
						processedLadders.clear();
						ladderHistograms.clear();
						regionsOfInterest.clear();
						computedRegions.clear();
						bookmarks.clear();
						
						userReferenceLadder.getMarks().clear();
					}
				});
				fileChooser.fadeIn();
				stage.addActor(fileChooser);
			}
		});
		
		final VisSlider smoothingSlider = new VisSlider(0.0f, 10.0f, 1.0f, false);
		smoothingSlider.addListener(new ChangeListener() {@Override
			public void changed(ChangeEvent event, Actor actor) {
				smoothingValue = smoothingSlider.getValue();
				
				if(setupLinks)
					bakeTextures();
			}
				
		});
		
		VisTextButton screenshotButton = new VisTextButton("Save Screenshot");
		screenshotButton.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				takeScreenshot = true;
			}
		});
		
		window.add(refLadderButton).padRight(20);
		window.add(refLadderLabel);
		window.row().pad(5);
		window.add(refActLadderButton).padRight(20);
		window.add(refActLadderLabel);
		window.row().pad(5);
		window.add(new VisLabel("Smoothing")).padRight(10.0f);
		window.add(smoothingSlider);
		window.row().pad(5);
		window.add(screenshotButton);
		window.pack();
			
		stage.addActor(window);
		Gdx.input.setInputProcessor(new InputMultiplexer(stage, new InputAdapter() {
			
			@Override
			public boolean keyDown (int keycode) {
				return false;
			}

			@Override
			public boolean keyUp (int keycode) {				
				if(keycode == Keys.SPACE) {
					if(setupLinks) {
						//setupLinks = false;
						
						formConnections();
					}
					
					if(setupComparison) {
						if(choosingRegions) {
							setupComparison = false;
							
							for(FrameBuffer f : processedLadders) {
								f.dispose();
							}
							
							for(FrameBuffer f : ladderHistograms) {
								f.dispose();
							}
							
							for(TextureRegion r : ladderRegions) {					
								processedLadders.add(new FrameBuffer(Format.RGBA8888, 64, 128, false));
								ladderHistograms.add(new FrameBuffer(Format.RGBA8888, 1, 128, false));
								ladderHistograms.get(ladderHistograms.size() - 1).getColorBufferTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
							}
							
							bakeTextures();
							
							setupLinks = true;
						}
					}
				}
				
				return false;
			}
			
			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				if(dragging) {
					if(setupComparison) {
		 				Rectangle selectedArea = new Rectangle(dragStart.x, dragStart.y, (mousePos.x - dragStart.x), (mousePos.y - dragStart.y));
						if(selectedArea.width < 0) {
							selectedArea.x += selectedArea.width;
							selectedArea.width *= -1;
						}
						
						if(selectedArea.height < 0) {
							selectedArea.y += selectedArea.height;
							selectedArea.height *= -1;
						}
						
						float x = VIRTUAL_WIDTH * 0.20f;
						float y = VIRTUAL_HEIGHT;
						float w = VIRTUAL_WIDTH * 0.80f;
						float h = VIRTUAL_HEIGHT;
							
						float mappedX = (selectedArea.x - x) / w;
						float mappedY = -((selectedArea.y - y) / h);
						float mappedWidth = ((selectedArea.x - x) + selectedArea.width) / w;
						float mappedHeight = -(((selectedArea.y - y) + selectedArea.height) / h);
						
						if(choosingRegions) {
							TextureRegion r = new TextureRegion(userLadder, MathUtils.lerp(cutRegion.getU(), cutRegion.getU2(), mappedX), cutRegion.getV(), MathUtils.lerp(cutRegion.getU(), cutRegion.getU2(), mappedWidth), cutRegion.getV2());
							ladderRegions.add(r);
							chosenRegions.add(selectedArea);
						}
						
						if(selectingArea) {
							selectingArea = false;
					
							cutRegion = new TextureRegion(userLadder, mappedX, mappedY, mappedWidth, mappedHeight);
							cutRegion.flip(false, true);
							
							choosingRegions = true;
						}
						
						dragging = false;
					}
				}
				
				if(setupLinks) {
					currentDrag = null;
					
					if(dragging) {
						System.out.println(Math.abs(newZoneBounds.height));
						
						if(Math.abs(newZoneBounds.height) > 0.1f) {
							if(newZoneBounds.height < 0) {
								newZoneBounds.y += newZoneBounds.height;
								newZoneBounds.height *= -1;
							}
							
							computedRegions.add(new Rectangle(newZoneBounds));
						}
						
						dragging = false;
					}
				}
				
				return false;
			}
		
			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				if(selectingArea && setupComparison) {
					if(!dragging) {
						dragStart.set(mousePos.cpy());
						dragging = true;
					}
				}
				
				if(choosingRegions && setupComparison) {
					if(!dragging) {
						dragStart.set(mousePos.cpy());
						dragging = true;
					}
				}
				
				if(setupLinks) {
					int count = ladderRegions.size();
					int breaks = count - 1;
					
					float spacing = 10.0f;
					
					float budget = ((VIRTUAL_WIDTH * 0.80f) - (breaks * spacing)) / (count);
					
					ArrayList<Rectangle> removeList = new ArrayList<Rectangle>();
					for(int i = 0; i < computedRegions.size(); i++) {
						Rectangle transformed = new Rectangle((VIRTUAL_WIDTH * 0.20f), (computedRegions.get(i).y - 1) * ((float)VIRTUAL_HEIGHT / 128.0f), budget, (computedRegions.get(i).height) * ((float)VIRTUAL_HEIGHT / 128.0f));
						
						if(transformed.contains(mousePos)) {
							if(button == Buttons.RIGHT) {
								removeList.add(computedRegions.get(i));
								break;
							}
							
							if(button == Buttons.LEFT) {
								currentDrag = computedRegions.get(i);
								zoneDragStart.set(mousePos.cpy());
								break;
							}
						}
					}
					computedRegions.removeAll(removeList);
					
					if(button == Buttons.LEFT) {
						if(currentDrag == null) {
							zoneDragStart.set(mousePos.cpy());
							dragging = true;
						}
					}
				}
				
				return false;
			}
		}));
		
		tempBuffer = new FrameBuffer(Format.RGB888, 256, 512, false);
		tempBuffer2 = new FrameBuffer(Format.RGB888, 256, 512, false);
	}
	
	FrameBuffer tempBuffer;
	FrameBuffer tempBuffer2;
	
	public void computeRegionsOfInterest() {
		bookmarks.clear();
		regionsOfInterest.clear();
		
		for(int i = 0; i < 1; i++) {					
			FrameBuffer histogramBuffer = ladderHistograms.get(i);
			FrameBuffer fullBuffer = processedLadders.get(i);
			
			histogramBuffer.bind();
			Pixmap histogram = ScreenUtils.getFrameBufferPixmap(0, 0, histogramBuffer.getWidth(), histogramBuffer.getHeight());
			FrameBuffer.unbind();
			
			fullBuffer.bind();
			Pixmap full = ScreenUtils.getFrameBufferPixmap(0, 0, fullBuffer.getWidth(), fullBuffer.getHeight());
			FrameBuffer.unbind();
			
			boolean basicDetection = false;

			for(int y = 0; y < histogram.getHeight(); y++) {
				Color pixel = new Color(histogram.getPixel(0, y));

				if(basicDetection) {
					if(pixel.r > 0.005f) {
						regionsOfInterest.add(new Vector3(0.2f, 1.0f, histogram.getHeight() - y));
						
						if(pixel.r > 0.1f) {
							regionsOfInterest.add(new Vector3(1.0f, 0.2f, histogram.getHeight() - y));
						}
					}
				} else {
					float midpointFeather = 0.45f;			
					int midpoint = full.getWidth() / 2;
					float samples = midpoint * midpointFeather;
					
					float leftHistogram = 0.0f;
					float rightHistogram = 0.0f;
					float fullHistogram = 0.0f;
					
					for(int j = 0; j < full.getWidth(); j++) {
						fullHistogram += new Color(full.getPixel(j, y)).r;
					}
					fullHistogram /= (float)full.getWidth();
					
					for(int j = 0; j < (int)samples; j++) {
						leftHistogram += new Color(full.getPixel(j, y)).r;
						rightHistogram += new Color(full.getPixel(full.getWidth() - j, y)).r;
					}
					leftHistogram /= (float)full.getWidth();
					rightHistogram /= (float)full.getWidth();
				
					float histogramThreshhold = 0.20f;
					float combinedHistogram = leftHistogram + rightHistogram;
					
					if(pixel.r > 0.005f) {
						if(fullHistogram * histogramThreshhold < leftHistogram | fullHistogram * histogramThreshhold < rightHistogram) {
							regionsOfInterest.add(new Vector3(1.0f, 0.2f, histogram.getHeight() - y));
						} else {
							regionsOfInterest.add(new Vector3(0.2f, 1.0f, histogram.getHeight() - y));
						}
					}
				}
			}
			
			histogram.dispose();
			full.dispose();
		
			if(regionsOfInterest.size() <= 0) return;
			
			ArrayList<Rectangle> computed = new ArrayList<Rectangle>();
			
			float start = regionsOfInterest.get(0).z;
			int index = 0;
			boolean done = false;
			
			while(!done) {
				for(int ii = index; ii < regionsOfInterest.size(); ii++) {
					float diff = Math.abs(start - regionsOfInterest.get(ii).z);

					if(diff > 1) {
						index = ii;
						start = regionsOfInterest.get(ii).z;
					}
					
					computed.add(new Rectangle(0, regionsOfInterest.get(ii).z, 1, 1.1f));	
					
					if(ii == regionsOfInterest.size() - 1) {
						done = true;
						break;
					}
				}
			}
		
			computedRegions.clear();
			
			done = false;
			Rectangle mark = computed.get(0);
			int loops = 0;
			
			
			while(true) {
				boolean startMerge = false;
				
				Rectangle merged = new Rectangle();
				for(int ii = 0; ii < computed.size(); ii++) {
					Rectangle b = computed.get(ii);
					
					if(startMerge) {
						if(!merged.overlaps(b)) {
							mark = b;
							break;
						} else {
							merged = merged.merge(b);
						}
					}
					
					if(b == mark && !startMerge) {
						startMerge = true;
						merged.set(b);
					}
					
					if(ii == computed.size() - 1) {
						startMerge = false;
					}
				}
			
				computedRegions.add(merged);
				
				if(!startMerge) {
					break;
				}
				
				if(mark == computed.get(computed.size() - 1)) {
					break;
				}
				
				if(loops++ >= computed.size()) {
					break;
				}
			}
		}
	}
	
	public void bakeTextures() {
		if(ladderRegions.size() <= 0) return;
		if(processedLadders.size() <= 0) return;
		
		//FrameBuffer downsampledBuffer = new FrameBuffer(Format.RGB888, 128, 512, false);
		OrthographicCamera camera = new OrthographicCamera(tempBuffer.getWidth(), tempBuffer.getHeight());
		camera.setToOrtho(false, tempBuffer.getWidth(), tempBuffer.getHeight());
		
		/*for(int i = 0; i < processedLadders.size(); i++) {
			if(processedLadders.get(i) != null)
				processedLadders.get(i).dispose();
		}*/
		
		for(int i = 0; i < 1; i++) {
			camera.setToOrtho(false, tempBuffer.getWidth(), tempBuffer.getHeight());
			
			FrameBuffer buffer = processedLadders.get(i);
			FrameBuffer histogramBuffer = ladderHistograms.get(i);
			//processedLadders.add(buffer = new FrameBuffer(Format.RGBA8888, downsampledBuffer.getWidth(), downsampledBuffer.getHeight(), false));
			
			
			tempBuffer.begin();
			{
				Gdx.gl.glClearColor(0, 0, 0, 1);
				Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
				
				Shaders.blurShader.begin();
				{
					Shaders.blurShader.setUniformf("u_resolution", ladderRegions.get(i).getRegionWidth(), ladderRegions.get(i).getRegionHeight());
					Shaders.blurShader.setUniformf("u_smoothing", smoothingValue);
				}
				Shaders.blurShader.end();
				
				batch.setProjectionMatrix(camera.combined);
				batch.begin();
				batch.setShader(Shaders.blurShader);
				{
					batch.draw(ladderRegions.get(i), 0, 0, tempBuffer.getWidth(), tempBuffer.getHeight());
				}
				batch.end();
			}
			tempBuffer.end();
			
			tempBuffer2.begin();
			{
				Gdx.gl.glClearColor(0, 0, 0, 1);
				Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
				
				batch.setProjectionMatrix(camera.combined);
				batch.begin();
				batch.setShader(Shaders.sobelShader);
				{
					batch.draw(tempBuffer.getColorBufferTexture(), 0, 0, tempBuffer2.getWidth(), tempBuffer2.getHeight());
				}
				batch.end();
			}
			tempBuffer2.end();
			
			camera.setToOrtho(false, buffer.getWidth(), buffer.getHeight());
			
			buffer.begin();
			{
				Gdx.gl.glClearColor(0, 0, 0, 1);
				Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
						
				batch.setProjectionMatrix(camera.combined);
				batch.begin();
				batch.setShader(null);
				{
					batch.draw(tempBuffer2.getColorBufferTexture(), 0, buffer.getHeight(), buffer.getWidth(), -buffer.getHeight());
				}
				batch.end();
			}
			buffer.end();
			
			camera.setToOrtho(false, histogramBuffer.getWidth(), histogramBuffer.getHeight());
			
			histogramBuffer.begin();
			{
				Gdx.gl.glClearColor(0, 0, 0, 1);
				Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
				
				batch.setProjectionMatrix(camera.combined);
				batch.begin();
				batch.setShader(Shaders.histogramShader);
				{
					batch.draw(tempBuffer2.getColorBufferTexture(), 0, 0, histogramBuffer.getWidth(), histogramBuffer.getHeight());
				}
				batch.end();
			}
			histogramBuffer.end();
		}
		
		computeRegionsOfInterest();
		//downsampledBuffer.dispose();
	}
	
	boolean overlay = false;
	
	@Override
	public void render() {
		if(Gdx.input.isKeyJustPressed(Keys.R)) {
			Assets.Load();
		}
		
		if(Gdx.input.isKeyJustPressed(Keys.T)) {
			overlay = !overlay;
		}
		
		
		//bakeTextures();
		
		Vector3 mouse3D = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
		mousePos.x = mouse3D.x;
		mousePos.y = mouse3D.y;
		
		
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.setProjectionMatrix(camera.combined);
		batch.enableBlending();
		batch.begin();
		{
			batch.setShader(null);
			if(ladders.get(0) != null) {
				ladders.get(0).render(batch);
			}
			
			if(userLadder != null) {
				if(setupComparison) { 
					if(cutRegion == null)
						batch.draw(userLadder, VIRTUAL_WIDTH * 0.20f, 0, VIRTUAL_WIDTH * 0.80f, VIRTUAL_HEIGHT);
					else
						batch.draw(cutRegion, VIRTUAL_WIDTH * 0.20f, 0, VIRTUAL_WIDTH * 0.80f, VIRTUAL_HEIGHT);
				} else {
					/*if(Gdx.input.isKeyJustPressed(Keys.A)) {
						overlay = !overlay;
					}*/
					
					int count = ladderRegions.size();
					int breaks = count - 1;
					
					float spacing = 10.0f;
					
					float budget = ((VIRTUAL_WIDTH * 0.80f) - (breaks * spacing)) / (count);
					if(overlay) {
						for(int i = 0; i < count; i++) {
							batch.draw(ladderRegions.get(i), (VIRTUAL_WIDTH * 0.20f) + (i * budget) + (i * (i == 0 ? 0.0f : 1.0f) * spacing), 0, budget, VIRTUAL_HEIGHT);
						}
						
						for(int i = 0; i < processedLadders.size(); i++) {
							batch.draw(processedLadders.get(i).getColorBufferTexture(), (VIRTUAL_WIDTH * 0.20f) + (i * budget) + (i * (i == 0 ? 0.0f : 1.0f) * spacing), 0, budget, VIRTUAL_HEIGHT);
						}
					} else {	
						for(int i = 0; i < processedLadders.size(); i++) {
							batch.draw(processedLadders.get(i).getColorBufferTexture(), (VIRTUAL_WIDTH * 0.20f) + (i * budget) + (i * (i == 0 ? 0.0f : 1.0f) * spacing), 0, budget, VIRTUAL_HEIGHT);
						}
						
						for(int i = 0; i < count; i++) {
							batch.draw(ladderRegions.get(i), (VIRTUAL_WIDTH * 0.20f) + (i * budget) + (i * (i == 0 ? 0.0f : 1.0f) * spacing), 0, budget, VIRTUAL_HEIGHT);
						}
					}
					
					for(int i = 0; i < ladderHistograms.size(); i++) {			
						batch.draw(ladderHistograms.get(i).getColorBufferTexture(), (VIRTUAL_WIDTH * 0.20f) + (i * budget) + (i * (i == 0 ? 0.0f : 1.0f) * spacing) - 7, 0, 4, VIRTUAL_HEIGHT);
					}
				}
			}
			
			batch.setShader(Shaders.textShader);
			
			for(MarkLink link : links) {
				lightFont.draw(batch, String.valueOf((int)link.value), link.x3 + 5, link.y2 - 5);
			}	
			
			if(referenceLadder != null && userReferenceLadder != null && links.size() > 0) {				
				int count = ladderRegions.size();
				int breaks = count - 1;
				
				float spacing = 10.0f;
				
				float budget = ((VIRTUAL_WIDTH * 0.80f) - (breaks * spacing)) / (count);
				
				for(Mark m : bookmarks) {
					Fonts.lightFont.setColor(Color.ORANGE);
					Fonts.lightFont.draw(batch, String.valueOf((int)m.getValue()), (VIRTUAL_WIDTH * 0.20f) + (0 * budget) + (0 * (0 == 0 ? 0.0f : 1.0f) * spacing) + budget + 20, (m.getFraction() * VIRTUAL_HEIGHT) + 20);
					Fonts.lightFont.setColor(Color.WHITE);
				}
				
				float fraction = mousePos.y / VIRTUAL_HEIGHT;
				Tuple<Mark> markBounds = userReferenceLadder.getBounds(fraction);
				
				if(markBounds.isComplete()) {	
					float value = (float) ((((1.0f - (markBounds.o2.getFraction() - fraction) / Math.abs(markBounds.o1.getFraction() - markBounds.o2.getFraction())) - 1.0f) * Math.abs(markBounds.o1.getValue() - markBounds.o2.getValue())) + markBounds.o2.getValue());
					Fonts.lightFont.setColor(Color.ORANGE);
					Fonts.lightFont.draw(batch, String.valueOf((int)value), (VIRTUAL_WIDTH * 0.20f) + (0 * budget) + (0 * (0 == 0 ? 0.0f : 1.0f) * spacing) + budget + 20, mousePos.y + 20);
					Fonts.lightFont.setColor(Color.WHITE);
					
					if(Gdx.input.isKeyJustPressed(Keys.A)) {
						Mark m = new Mark();
						m.setFraction(fraction);
						m.setValue(value);
						bookmarks.add(m);
					}
					
					if(Gdx.input.isKeyJustPressed(Keys.Z)) {
						if(bookmarks.size() > 0) {
							bookmarks.remove(bookmarks.size() - 1);
						}
					}
				}
			}
		}
		batch.end();

		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		
		sr.setProjectionMatrix(camera.combined);
		sr.begin();
		
		sr.setColor(Color.GREEN);
		{
			sr.set(ShapeType.Line);
			{
				sr.setColor(Color.ORANGE);
				for(MarkLink link : links) {
					link.renderLink(sr);
				}
				
				if(referenceLadder != null && userReferenceLadder != null && links.size() > 0) {
					int count = ladderRegions.size();
					int breaks = count - 1;
					
					float spacing = 10.0f;
					
					float budget = ((VIRTUAL_WIDTH * 0.80f) - (breaks * spacing)) / (count);
					
					sr.setColor(Color.RED);
					for(Mark m : bookmarks) {
						sr.line((VIRTUAL_WIDTH * 0.20f) + (0 * budget) + (0 * (0 == 0 ? 0.0f : 1.0f) * spacing) + budget + 20, m.getFraction() * VIRTUAL_HEIGHT, VIRTUAL_WIDTH, m.getFraction() * VIRTUAL_HEIGHT);
					}
					
					sr.line((VIRTUAL_WIDTH * 0.20f) + (0 * budget) + (0 * (0 == 0 ? 0.0f : 1.0f) * spacing) + budget + 20, mousePos.y, VIRTUAL_WIDTH, mousePos.y);
					
				}
			}
			
			sr.set(ShapeType.Filled);
			{
				if(setupComparison) {
					if(selectingArea && dragging) {
						sr.setColor(0, 1, 0, 0.3f);
						sr.rect(dragStart.x, dragStart.y, (mousePos.x - dragStart.x), (mousePos.y - dragStart.y));
					}
					
					if(choosingRegions && dragging) {
						sr.setColor(0, 1, 0, 0.3f);
						sr.rect(dragStart.x, 0, (mousePos.x - dragStart.x), VIRTUAL_HEIGHT);
					}		
					
					for(Rectangle r : chosenRegions) {
						sr.setColor(0.8f, 1, 0.5f, 0.3f);
						sr.rect(r.x, 0, r.width, VIRTUAL_HEIGHT);
					}
				}
				
				sr.setColor(Color.YELLOW);
				int count = ladderRegions.size();
				int breaks = count - 1;
				
				float spacing = 10.0f;
				
				float budget = ((VIRTUAL_WIDTH * 0.80f) - (breaks * spacing)) / (count);
				
				if(setupLinks) {
					if(dragging) {
						sr.setColor(0, 1, 0, 0.3f);
						sr.rect((VIRTUAL_WIDTH * 0.20f) + (0 * budget) + (0 * (0 == 0 ? 0.0f : 1.0f) * spacing), zoneDragStart.y, budget, (mousePos.y - zoneDragStart.y));
						newZoneBounds.set(0, ((((mousePos.y - (mousePos.y - zoneDragStart.y)) / VIRTUAL_HEIGHT)) * 128.0f) + 1.0f, 0, (((mousePos.y - zoneDragStart.y) / VIRTUAL_HEIGHT) * 128.0f));
					}
				}
				
				for(int i = 0; i < regionsOfInterest.size(); i++) {
					sr.setColor(regionsOfInterest.get(i).x, regionsOfInterest.get(i).y, 0.0f, 1.0f);
					sr.rect((VIRTUAL_WIDTH * 0.20f) - 11, (regionsOfInterest.get(i).z - 1) * ((float)VIRTUAL_HEIGHT / 128.0f), 4, ((float)VIRTUAL_HEIGHT / 128.0f));
				}
				
				if(currentDrag != null) {
					currentDrag.y = ((mousePos.y / VIRTUAL_HEIGHT)) * 128.0f;
				}
				
 				for(int i = 0; i < computedRegions.size(); i++) {
					Rectangle transformed = new Rectangle((VIRTUAL_WIDTH * 0.20f), (computedRegions.get(i).y - 1) * ((float)VIRTUAL_HEIGHT / 128.0f), budget, (computedRegions.get(i).height) * ((float)VIRTUAL_HEIGHT / 128.0f));
					
					if(transformed.contains(mousePos)) {
						sr.setColor(0.8f, 1, 0.5f, 0.3f);							
					}
					else {
						sr.setColor(0.4f, 1, 0.2f, 0.3f);
					}
					
					if(computedRegions.get(i) == currentDrag) {
						sr.setColor(0.1f, 0.2f, 0.6f, 0.3f);
					}
					
					sr.rect(transformed.x, transformed.y, transformed.width, transformed.height);
				}
			}
		}
		sr.end();
	
	   stage.act(Gdx.graphics.getDeltaTime());
	   if(!takeScreenshot) stage.draw();
	   
	   if(takeScreenshot) {
		   if(referenceLadder != null && userReferenceLadder != null && refName != null & urefName != null) {
	           FileHandle fh;
	           Date date = new Date(TimeUtils.millis());
	           do {
	               fh = new FileHandle(Gdx.files.internal("screenshots/" + refName + "_" + urefName + "_" + date.getTime()) + ".png");
	           } while (fh.exists());
			   
	           Pixmap screen = ScreenUtils.getFrameBufferPixmap(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	           Pixmap flipped = flipPixmap(screen);
	           
			   PixmapIO.writePNG(fh, flipped);
			   
			   flipped.dispose();
			   screen.dispose();
		   }
		   
		   takeScreenshot = false;
	   }
	}
	
	public Pixmap flipPixmap(Pixmap src) {
	    final int width = src.getWidth();
	    final int height = src.getHeight();
	    Pixmap flipped = new Pixmap(width, height, src.getFormat());

	    for (int x = 0; x < width; x++) {
	        for (int y = 0; y < height; y++) {
	        	flipped.drawPixel(x, y, src.getPixel(x, height - y - 1));
	        }
	    }
	    return flipped;
	}
	
	public void formConnections() {		
		if(referenceLadder == null) return;
		
		bookmarks.clear();
		
		Collections.sort(computedRegions, new Comparator<Rectangle>() {
			@Override
			public int compare(Rectangle o1, Rectangle o2) {
				return Float.compare(o2.y, o1.y);
			}
		});
		
		links.clear();
		userReferenceLadder.getMarks().clear();
		
		int linkCount = Math.min(referenceLadder.getLadder().getMarks().size(), computedRegions.size());
		
		for(int i = 0; i < linkCount; i++) {
			Mark currentMark = referenceLadder.getLadder().getMarks().get(i);
		
			float x = referenceLadder.getPosition().x;
			float x2 = referenceLadder.getPosition().x + referenceLadder.getSize().x;
			float y = referenceLadder.getSize().y * currentMark.getFraction();
			float fraction = (Math.abs((computedRegions.get(i).y + computedRegions.get(i).height * 0.5f) - 1)) / 128.0f;
			float x3 = VIRTUAL_WIDTH * 0.15f;
			float x4 = VIRTUAL_WIDTH * 0.20f;
			float y2 = (Math.abs((computedRegions.get(i).y + computedRegions.get(i).height * 0.5f) - 1) * VIRTUAL_HEIGHT) / 128.0f;
			
			MarkLink link = new MarkLink();
			link.x = x;
			link.x2 = x2;
			link.y = y;
			link.x3 = x3;
			link.x4 = x4;
			link.y2 = y2;
			link.value = currentMark.getValue(); 
			
			links.add(link);
			
			Mark mark = new Mark();
			mark.setFraction(fraction);
			mark.setValue(currentMark.getValue());
			
			userReferenceLadder.getMarks().add(mark);
		}
		
		userReferenceLadder.sortMarks();
	}
	
	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void dispose() {
	}
}
