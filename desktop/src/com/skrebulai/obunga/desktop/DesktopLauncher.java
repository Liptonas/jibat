package com.skrebulai.obunga.desktop;

//import org.opencv.core.Core;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.skrebulai.obunga.Main;

import net.spookygames.gdx.nativefilechooser.desktop.DesktopFileChooser;

public class DesktopLauncher {
	public static void main (String[] arg) {
		//System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.width = 1200;
		config.height = 900;
		
		new LwjglApplication(new Main(new DesktopFileChooser()), config);
	}
}
